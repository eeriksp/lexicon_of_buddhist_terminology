﻿# Lexicon of Buddhist terminology

A Buddhist terminology application built with the Django Python web framework. Working app can currently be found [here](http://eerik.pythonanywhere.com).



## Mission

The aim of this application is to contribute to the elaboration of Estonian Buddhist terminology, provide a record of Buddhist literature available in Estonian and offer a list of Estonian sanghas. 

The dictionary is a community project open for everyone to suggest alternative translations and other improvements.



## Documentation

Guidelines about technical issues (such as server management) can be found in the [docs](https://gitlab.com/eerik.sven/lexicon_of_buddhist_terminology/tree/fcb0f1dd9a06c50ace758a1e5a4305ba2cff908a/docs) 
directory (in English). Guidelines for using the application can be found [here] (https://gitlab.com/eerik.sven/lexicon_of_buddhist_terminology/tree/master/src/general/md) (in Estonian).



## Special thanks

Special thanks to:



* Venerable Ṭhitañāṇo Bhikkhu for the initial idea;

* Mart Sõmermaa for great ideas and technical support.