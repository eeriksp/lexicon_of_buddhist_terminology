# Managing PythonAnywhere server

## Pulling changes from GitLab to the server

1. Open a bash console in PythonAnywhere.
1. Go to the project's repository: `cd ./lexicon_of_buddhist_terminology`
1. Type `git pull`
1. Type `python ./manage.py makemigrations`
1. Type `python ./manage.py migrate`
1. Reload to application:
    1. Go to dashboard;
    1. From the top menu choose 'Web';
    1. Hit the green reload button.

## Downloading new packages to the server

1. Open a bash console in your computer.
2. Go to the project's src directory
2. Update requirements.txt `pip freeze > requirements.txt`
3. Control whether the update was successful `cat requirements.txt`
3. Push changes to GitLab
4. Open a bash console in PythonAnywhere.
5. Go to the project's repository: `cd ./lexicon_of_buddhist_terminology`
6. Type `git pull`
7. Type `pip install -r requirements.txt`
