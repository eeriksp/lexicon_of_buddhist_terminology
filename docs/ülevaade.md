# Projekti ülevaade

## Eesmärgid
Projekti eesmärgiks on anda oma panus ühtse eestikeelse budistliku terminoloogia väljakujunemisse, olles platformiks, kus asjaosalised saavad erinevaid tõlkevasteid arutada. Süsteem võimaldab ühele terminile lisada mitu samakeelset vastet, seega ei pea eestikeelseid tõlkevasteid tingimata olema ainult üks.

## Ülevaade
1. Projekt tegeleb ainult budistliku terminoloogiaga.
1. Projekt ei ole seotud ühegi kindla koolkonnaga ega eelista ühe koolkonna vaateid teistele.
1. Tegemist on kogukonnapõhise projektiga, mis on kõigile kaasarääkimiseks avatud. Leksikoni kirjete muutmiseks on tarvis registreeruda kasutajaks ning taotleda administraatoritelt vastavad õigused.

## Kasutajad ja nende õigused
Iga järgnev kasutajarühm omab ka kõikide eelnenud rühmade õigusi.
1. **Registreerumata kasutaja:** saab kommenteerida ja lehe administraatroitega ühendust võtta;
2. **Tavakasutaja:** saab kommenteerida ja lehe administraatroitega ühendust võtta, ilma et peaks iga kord oma nime ja e-maili uuesti sisestama;
3. **Kaasautor:** saab muuta leksikoni kandeid;
4. **Administraator (haldaja):** saab kasutajale anda ja ära võtta kaasautori õigusi, saab kustutada kommentaare;
5. **Arendaja:** omab piiramatut ligipääsu kõikidele projketiga seonduvatele failidele ning serverile.

## Kommenteerimine
1. Kommenteerida saavad kõik lehe külastajad, sisselogimine ei ole nõutav.
2. Kommentaarid peavad olema teemakohased, hoiduda tuleb ründavast ja toorest kõnepruugist. Mittenõustumist tuleb väljendada elegantselt.
3. Administraator võib kommentaari kustutada, kui
  - see ei ole teemakohane;
  - kommentaaris tehtud parandusettepanekud on arvesse võetud, kommentar on oma eesmärgi täitnud;
  - kommenteeria väljendab ühte ja sama mõtet mitmes kommentaris;
  - kasutatud on sobimatut kõnepruuki.