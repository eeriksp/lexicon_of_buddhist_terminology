from django import forms
from django.contrib.auth.models import User


class NewUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'email')

class LogInForm(forms.Form):
    username = forms.CharField(initial='kasutajatunnus')
    password = forms.CharField(initial='salasõna')

class LogOutForm(forms.Form):
    pass