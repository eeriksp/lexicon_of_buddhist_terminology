from django.apps import AppConfig


class KasutajadConfig(AppConfig):
    name = 'kasutajad'
