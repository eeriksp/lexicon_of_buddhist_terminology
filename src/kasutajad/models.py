from django.contrib.auth.models import User
from django.db import models


class UserAdds(models.Model):
    user = models.OneToOneField(User)
    slug = models.SlugField()

    def __str__(self):
        return self.slug
