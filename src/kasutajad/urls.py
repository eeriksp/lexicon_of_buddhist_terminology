from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^sisselogimine/$', views.log_in, name='log_in'),
    url(r'^kasutaja/(?P<user_slug>[\w+]+)/$', views.user_page, name='user_page'),

]
