from django.shortcuts import render
from .models import Sangha

def list_of_sanghas(request):
    sanghas = Sangha.objects.all()
    return render(request, 'sanghas/list_of_sanghas.html', {'sanghas': sanghas})
