from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.homepage, name='homepage'),
    url(r'^keelatud/$', views.no_permission, name='no_permission'),
    url(r'^otsi/$', views.search, name='search'),
]
