# Lugejale

## Lühendid

* pl – paali keeles
* skr – sanskriti keeles
* hn – hiina keeles
* jp – jaapani keeles
* tb – tiibeti keeles
* eng – inglise keeles

## Kommenteerimine
1. Kommenteerida saavad kõik lehe külastajad, sisselogimine ei ole nõutav.
2. Kommentaarid peavad olema teemakohased, hoiduda tuleb ründavast ja toorest kõnepruugist. Mittenõustumist tuleb väljendada elegantselt.
3. Administraator võib kommentaari kustutada, kui
    - see ei ole teemakohane;
    - kommentaaris tehtud parandusettepanekud on arvesse võetud, kommentar on oma eesmärgi täitnud;
    - kommenteerija väljendab ühte ja sama mõtet mitmes kommentaaris;
    - kasutatud on sobimatut kõnepruuki.

