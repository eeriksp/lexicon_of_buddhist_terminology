from django import template

from general.models import LetterToAdmin

register = template.Library()


@register.filter()
def letter_to_admin_number(pointless_var):
    return len(LetterToAdmin.objects.all())
