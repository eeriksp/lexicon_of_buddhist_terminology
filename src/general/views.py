import os
from django.contrib import messages
from django.utils.text import slugify
from terminology.models import InEstonian, InEnglish, InTibetan, InJapanese, InChinese, InSanskrit, InPali

from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404, redirect
from markdown import markdown

from .forms import LetterToAdminAnonForm, LetterToAdminUserForm


def homepage(request):
    return render(request, 'general/homepage.html', {})


def for_contributor(request):
    md_content_as_html = _load_md_file_as_html('for_contributor.md')
    return render(request, 'general/for_reader.html', {'md_content_as_html': md_content_as_html})


def for_reader(request):
    md_content_as_html = _load_md_file_as_html('for_reader.md')
    return render(request, 'general/for_reader.html', {'md_content_as_html': md_content_as_html})


def no_permission(request):
    return render(request, 'general/no_permission.html', {})


LETTER_SENT = 'Kiri saadeti teele.'


def letter_to_admin(request):
    if request.method == "POST":
        if request.user.is_authenticated == True:
            form = LetterToAdminUserForm(request.POST)
        else:
            form = LetterToAdminAnonForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            if request.user.is_authenticated == True:
                form.user = request.user
                form.sender = request.user.username
                form.email = request.user.email
            form.save()
            return render(request, 'general/letter_to_admin.html', {'form': form, 'message': LETTER_SENT})
    else:
        if request.user.is_authenticated == True:
            form = LetterToAdminUserForm()
        else:
            form = LetterToAdminAnonForm()
    return render(request, 'general/letter_to_admin.html', {'form': form, 'message': ''})


def _load_md_file_as_html(file_name):
    path = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(path, 'md', file_name)
    md = open(path, 'r', encoding='utf-8')
    content = md.read()
    return markdown(content)


def search(request):
    query = request.GET.get('q')
    if query:
        results = list(InEstonian.objects.filter(translation__icontains=query)) + \
                  list(InSanskrit.objects.filter(translation__icontains=query)) + \
                  list(InPali.objects.filter(translation__icontains=query)) + \
                  list(InChinese.objects.filter(translation__icontains=query)) + \
                  list(InJapanese.objects.filter(translation__icontains=query)) + \
                  list(InTibetan.objects.filter(translation__icontains=query)) + \
                  list(InEnglish.objects.filter(translation__icontains=query))
    elif 'HTTP_REFERER' in request.META:
        messages.info(request, 'Üritasid sooritada otsingut ilma otsingusõna sisestamata.')
        return redirect(request.META['HTTP_REFERER'])
    else:
        messages.info(request, 'Üritasid sooritada otsingut ilma otsingusõna sisestamata.')
        return redirect('homepage')

    if len(results) == 0:
        query = slugify(query)
        results = list(InEstonian.objects.filter(slug__icontains=query)) + \
                  list(InSanskrit.objects.filter(slug__icontains=query)) + \
                  list(InPali.objects.filter(slug__icontains=query)) + \
                  list(InChinese.objects.filter(slug__icontains=query)) + \
                  list(InJapanese.objects.filter(slug__icontains=query)) + \
                  list(InTibetan.objects.filter(slug__icontains=query)) + \
                  list(InEnglish.objects.filter(slug__icontains=query))

    if len(results) == 1:
        return redirect('term_detail', id=results[0].term.id)
    elif len(results) == 0:
        messages.info(request, 'Ühtegi vastet ei leitud.')
        return redirect(request.META['HTTP_REFERER'])
    else:
        return render(request, 'general/search.html', {'results': results,
                                                       'message': 'Sinu otsingule vastab mitu tulemust.'})
