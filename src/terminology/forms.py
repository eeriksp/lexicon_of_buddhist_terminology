from django import forms

from .models import Comment


class CommentUserForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('body',)
        labels = {'body': 'Sõnum'}


class CommentAnonForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('sender', 'email', 'body',)
        labels = {'sender': 'Nimi', 'email': 'E-mail', 'body': 'Sõnum'}


