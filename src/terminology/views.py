import re

from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone

from .models import Term, AlternativeExplanations, Comment, InEstonian, InEnglish, InTibetan, InJapanese, InChinese, \
    InPali, InSanskrit

from .forms import CommentUserForm, CommentAnonForm

REGEX = re.compile(r'\[\[(\d+)(#|\|)(\w+)\]\]')


def term_detail(request, id):
    id = int(id)
    term = get_object_or_404(Term, id=id)
    term.explanation = REGEX.sub(r'<a href="/terminid/\1/">\3</a>', term.explanation)
    title = term.inestonian_set.filter(is_best=True)
    if title:
        title = title[0].translation.capitalize()
    else:
        title = term.inestonian_set.all()[0].translation.capitalize()
    alt_expl = AlternativeExplanations.objects.filter(term=term)
    comments = list(Comment.objects.filter(term=term))
    if request.method == "POST":
        if request.user.is_authenticated:
            form = CommentUserForm(request.POST)
            if form.is_valid():
                form = form.save(commit=False)
                form.term = term
                form.user = request.user
                form.date = timezone.now()
                form.sender = request.user.username
                form.email = request.user.email
                form.save()
                return redirect('term_detail', id=id)
        else:
            form = CommentAnonForm(request.POST)
            if form.is_valid():
                form = form.save(commit=False)
                form.term = term
                form.date = timezone.now()
                form.save()
                return redirect('term_detail', id=id)
    else:
        if request.user.is_authenticated:
            form = CommentUserForm()
        else:
            form = CommentAnonForm()
    return render(request, 'terminology/term.html',
                  {'term': term, 'alt_trans': alt_expl, 'comments': comments, 'form': form,
                   'title': title,})


LANG_OPTIONS = {'est': 'in_estonian',
                'pl': 'in_pali',
                'skr': 'in_sanskrit',
                'hn': 'in_chinese',
                'jp': 'in_japanese',
                'tb': 'in_tibetan',
                'eng': 'in_english'}


def term_list(request, lang):
    if lang == 'est':
        words = InEstonian.objects.all()
    elif lang == 'pl':
        words = InPali.objects.all()
    elif lang == 'skr':
        words = InSanskrit.objects.all()
    elif lang == 'hn':
        words = InChinese.objects.all()
    elif lang == 'jp':
        words = InJapanese.objects.all()
    elif lang == 'tb':
        words = InTibetan.objects.all()
    elif lang == 'eng':
        words = InEnglish.objects.all()
    return render(request, 'terminology/term_list.html', {'words': words})

"""
terms = Term.objects.all().order_by(LANG_OPTIONS[lang]).values()
term_in_lang = [t[LANG_OPTIONS[lang]] for t in terms]
term_li = []
for t in terms:
    term_li.append((t[LANG_OPTIONS[lang]], t['slug']))
return render(request, 'terminology/term_list.html', {'term_in_lang': term_in_lang, 'term_li': term_li})
"""
